#include <stdio.h>
#include <string.h>

void hexify (FILE *out, FILE *inp, const char* func_name)
{

  fprintf(out, "const char %s[] = {", func_name);

  for (int len = 0, ch = 0; (ch = fgetc(inp)) != EOF; len++) {

    if (len % 12 == 0) {
      fputs("\n  ", out) ;
    } else {
      fputs(" ", out);
    }

    fprintf(out, "0x%02x,", ch);
  }

  fputs("\n0};\n", out);
}


int main (int argc, char *argv[])
{
  FILE *inp, *out;
  static char *output_base = "src_turnkey";
  static char output_file[256];

  if (argc < 2 || argc > 3 ) {
    puts("Usage: make_turnkey file.fs [output_base]");
    return 0;
  }

  if (!(inp = fopen(argv[1], "r"))) {
    fprintf(stderr, "Can't open %s for reading!\n", argv[1]);
    return 1;
  }

  if (argc == 3) output_base = argv[2];
  strncpy(output_file, output_base, sizeof output_file - 6);
  strcat(output_file, ".c");

  if (!(out = fopen(output_file, "w"))) {
    fprintf(stderr, "Can't open %s for writing!\n", output_file);
    return 1;
  }

  hexify(out, inp, output_base);

  fclose(out);
  fclose(inp);
  return 0;
}
