CC?=clang
CFLAGS?=-Wall -Wno-unused -Wno-unused-result -fno-inline -O2 -g
FEATURES=-DLIB_SHELL -DLIB_REGEX -DLIB_FORK -DEXTRAS


.PHONY: all base examples clean cleanall bench test compare
.PHONY: shell editor tools cgi rts

base: reforth rf reforth_debug
all: base examples

examples: editor shell wordprocessor gmenu cgi rts

mkturnkey: mkturnkey.c
	$(CC) $(CFLAGS) mkturnkey.c -o $@

src_base.c: base.fs mkturnkey
	./mkturnkey base.fs src_base

reforth: reforth.c src_base.c
	$(CC) $(CFLAGS) $(FEATURES) reforth.c -o $@
	objdump -d reforth >reforth.dump
	strip reforth

reforth_debug: reforth.c src_base.c
	$(CC) $(CFLAGS) $(FEATURES) -DDEBUG reforth.c -o $@

TURNKEYDEPS=reforth.c src_base.c mkturnkey

shell: rf
rf: shell.fs $(TURNKEYDEPS)
	./mkturnkey shell.fs
	$(CC) $(CFLAGS) $(FEATURES) -DTURNKEY reforth.c -o $@
	strip rf

editor: re
re: editor.fs $(TURNKEYDEPS)
	./mkturnkey editor.fs
	$(CC) $(CFLAGS) $(FEATURES) -DTURNKEY reforth.c -o $@
	strip re

tools: gmenu
gmenu: gmenu.fs $(TURNKEYDEPS)
	./mkturnkey gmenu.fs
	$(CC) $(CFLAGS) $(FEATURES) -DTURNKEY reforth.c -o $@

wordprocessor: rp
rp: prose.fs $(TURNKEYDEPS)
	./mkturnkey prose.fs
	$(CC) $(CFLAGS) $(FEATURES) -DTURNKEY reforth.c -o $@

cgi: web
web: web.fs $(TURNKEYDEPS)
	./mkturnkey web.fs
	$(CC) $(CFLAGS) $(FEATURES) -DTURNKEY reforth.c -o $@

rts: rg
rg: rts.fs $(TURNKEYDEPS)
	./mkturnkey rts.fs
	$(CC) $(CFLAGS) $(FEATURES) -DTURNKEY reforth.c -o $@

bench:  mkturnkey
	./mkturnkey test.fs
	gcc $(CFLAGS) $(FEATURES) -DTURNKEY reforth.c -o test_gcc
	clang $(CFLAGS) $(FEATURES) -DTURNKEY reforth.c -o test_clang
	$(SHELL) -c "time ./test_gcc"
	$(SHELL) -c "time ./test_clang"

compare:
	gcc $(CFLAGS) $(FEATURES) reforth.c -o reforth_gcc
	clang $(CFLAGS) $(FEATURES) reforth.c -o reforth_clang
	objdump -d reforth_gcc >reforth_gcc.dump
	objdump -d reforth_clang >reforth_clang.dump

test: reforth
	valgrind ./reforth test.fs

clean:
	rm -f src_turnkey.c mkturnkey reforth_* \
	re rg rp web gmenu \
	re *~ default.txt *.dump test_*
	rm -f reforth rf
